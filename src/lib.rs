pub mod squirrel3 {
    
    use std::num::Wrapping;

    const BIT_NOISE1: Wrapping<u32> = Wrapping(0xb5297a4d);
    const BIT_NOISE2: Wrapping<u32> = Wrapping(0x68e31da4);
    const BIT_NOISE3: Wrapping<u32> = Wrapping(0x1b56c4e9);

    #[inline]
    pub fn noise(position: u32, seed: u32) -> u32 {
        let mut mangled: Wrapping<u32> = Wrapping(position);

        mangled *= BIT_NOISE1;
        mangled += Wrapping(seed);
        mangled ^= mangled >> 8;
        mangled += BIT_NOISE2;
        mangled ^= mangled << 8;
        mangled *= BIT_NOISE3;
        mangled ^= mangled >> 8;
        mangled.0
    }

    const PRIME_2D_NOISE: Wrapping<u32> = Wrapping(198491317);

    #[inline]
    pub fn noise2d(position_x: u32, position_y: u32, seed: u32) -> u32 {
        let position: u32 = (Wrapping(position_x) + (PRIME_2D_NOISE * Wrapping(position_y))).0;
        noise(position, seed)
    }
}


#[cfg(test)]
mod test {
    use crate::squirrel3::noise;

    #[test]
    fn it_works() {
        assert_eq!(436901570, noise(0, 0));
    }
}
