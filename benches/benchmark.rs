use criterion::black_box;
use criterion::criterion_group;
use criterion::criterion_main;
use criterion::Criterion;
use rayon::prelude::*;
use squirrel_noise_generator::squirrel3::noise;

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("Noise benchmarks");
    let seed: u32 = 4294967295;

    group.bench_function("0 - 1000000 u32.", |b| {
        b.iter(|| {
            (0u32..100000u32)
                .into_par_iter()
                .map(|position| black_box(noise(position, seed)))
                .count();
        })
    });

    group.bench_function("2146983647 - 2147983647 u32.", |b| {
        b.iter(|| {
            (2146983647u32..2147983647u32)
                .into_par_iter()
                .map(|position| black_box(noise(position, seed)))
                .count();
        })
    });

    group.bench_function("4293967295 - 4294967295 u32.", |b| {
        b.iter(|| {
            (4293967295u32..4294967295u32)
                .into_par_iter()
                .map(|position| black_box(noise(position, seed)))
                .count();
        })
    });
    group.finish()
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
